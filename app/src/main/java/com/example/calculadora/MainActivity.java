package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    EditText  texto;
    Button b0,b2,b1,b3,b4,b5,b6,b7,b8,b9,bcoma,bdiv,bmult,bres,bsum,blimp,bigual;
    String  operacion ="";
    double  num1,num2;
    double  res=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        texto=findViewById(R.id.edit_text);

        b0=findViewById(R.id.b_0);
        b0.setOnClickListener(this);
        b1=findViewById(R.id.b_1);
        b1.setOnClickListener(this);
        b2=findViewById(R.id.b_2);
        b2.setOnClickListener(this);
        b3=findViewById(R.id.b_3);
        b3.setOnClickListener(this);
        b4=findViewById(R.id.b_4);
        b4.setOnClickListener(this);
        b5=findViewById(R.id.b_5);
        b5.setOnClickListener(this);
        b6=findViewById(R.id.b_6);
        b6.setOnClickListener(this);
        b7=findViewById(R.id.b_7);
        b7.setOnClickListener(this);
        b8=findViewById(R.id.b_8);
        b8.setOnClickListener(this);
        b9=findViewById(R.id.b_9);
        b9.setOnClickListener(this);
        bcoma=findViewById(R.id.b_coma);
        bcoma.setOnClickListener(this);
        bdiv=findViewById(R.id.b_div);
        bdiv.setOnClickListener(this);
        bmult=findViewById(R.id.b_mul);
        bmult.setOnClickListener(this);
        bres=findViewById(R.id.b_menos);
        bres.setOnClickListener(this);
        bsum=findViewById(R.id.b_mas);
        bsum.setOnClickListener(this);
        bigual=findViewById(R.id.b_igual);
        bigual.setOnClickListener(this);
        blimp=findViewById(R.id.b_borrar);
        blimp.setOnClickListener(this);








    }

    @Override
    public void onClick(View v) {
        Button b = (Button) v;
        String opcion =b.getText().toString();
        System.out.println(opcion);

        switch (opcion){

            case "+":

                num1 =Double.parseDouble(String.valueOf(texto.getText()));
                afegir(opcion);
                operacion ="+";
                break;
            case "-":

                num1 =Double.parseDouble(String.valueOf(texto.getText()));
                afegir(opcion);
                operacion ="-";
                break;

            case "*":

                num1 =Double.parseDouble(String.valueOf(texto.getText()));
                afegir(opcion);
                operacion ="*";
                break;

            case "/":

                num1 =Double.parseDouble(String.valueOf(texto.getText()));
                afegir(opcion);
                operacion ="/";
                break;

            case "=":

               num2= obtenernum2(operacion);

                switch (operacion) {
                    case "+":
                        res=suma(num1, num2);
                        texto.setText(String.valueOf(res));
                        break;
                    case "-":
                        res=resta(num1, num2);
                        texto.setText(String.valueOf(res));
                        break;
                    case "*":
                        res=multiplicacion(num1, num2);
                        texto.setText(String.valueOf(res));
                        break;
                    case "/":
                        res=division(num1, num2);
                        texto.setText(String.valueOf(res));
                        break;
                }
                break;
            case "C":
                num2=0;
                num1=0;
                texto.setText("");
                break;

            default:
                afegir(opcion);

        }




    }

    public double suma (double n1,double  n2){
        return n1+n2;
    }
    public double resta (double n1,double  n2){

        return n1-n2;
    }
    public double multiplicacion (double n1,double  n2){

        return n1*n2;
    }

    public double division (double n1,double  n2){

        return n1/n2;
    }


    public void afegir (String str){
        String  frase = texto.getText().toString();
        texto.setText(frase.concat(str));
    }
    public double obtenernum2(String  op){

        String f2=String.valueOf(texto.getText());
        System.out.println(f2);
        String [] fnum2=f2.split(getString(R.string.barras).concat(op));
        System.out.println(Arrays.toString(fnum2));
        num2=Double.parseDouble(fnum2[1]);
        return num2;

    }

}